void setupAccessPoint(void)
{
  Serial.println("Turning On HotSpot");
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.softAP("LocalServer@192.168.4.1", "");
  Serial.println("Initializing WiFi AccessPoint");
}