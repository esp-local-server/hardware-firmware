const char INDEX_HTML[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Games 365</title>
  <style>
  *{
    margin: 5px;
    padding: 5px;
    box-sizing: border-box;
}

img{
    width: 100%;
    height: 100%;
    max-height: 500px; 
    max-width: 500px;
}

h1{
    padding-top: 10px !important; 
    font-size: 3.5em; 
    font-family: webnar-bold;
}
ul{
    padding: 5px;
    display: table;
    width: 100%;
    list-style: none;
}

a{
    text-decoration: none;
    color: #c6c6c6;
}

.top-tagline-banner{
    width: 100%;
    display: inline-flex;
    padding: 10px;
    margin: auto;
    justify-content: space-evenly;
}

.about-us{
    width: 100%;
    object-position: center;
    padding: 10px;
}

.pricing-cards-wrapper{
    padding: 10px;
    display: inline-flex;
    width: 100%; 
    margin: auto;
}
.pricing-card{
    border: solid 1px; 
    padding: 10px;
}
.pricing-cards-body{
    border: solid 1px; 
    padding: 10px; 
    background-color: #131313;
    margin: auto;
    height: 100%;
    width: 100%;
}
.price-tag{
    text-align:center; 
    font-size:1.5em;
}
.pricing:hover{
    transition: all .3s ease-in-out;
}

.external-links li {
    display: table-cell;
    text-align: center;
}


@media screen and (max-width: 768px) {
    *{
        margin: 2px;
        padding: 2px;
    }

    .about-us{
        width: 100%;
        object-position: center;
        padding: 10px;
    }

    .pricing-cards-wrapper{
        padding: 10px;
        display: block;
        width: 100%; 
        margin: auto;
    }

    .pricing-cards-body{
        border: solid 1px; 
        padding: 10px; 
        text-align: center;
    }

    .top-tagline-banner{
        padding: 10px;
        margin: 10px;
        display: inline-block;
        margin: auto;
    }

    .external-links{
        width: 100%;
        list-style: none;
    }
    .external-links li {
        padding: 5px;
        display: block;
        text-align: center;
    }

}

@media screen and (min-width: 769px) and (max-width: 1200px) {
    *{
        margin: 2px;
        padding: 2px;
    }

     .pricing-cards-wrapper{
        padding: 10px;
        display: block;
        width: 100%; 
        margin: auto;
    }
    
    .pricing-cards-body{
        border: solid 1px; 
        padding: 10px; 
        text-align: center;
    }

    .top-tagline-banner{
        padding: 10px;
        margin: 10px;
        display: flex;
        margin: auto;
    }
    ul{
        padding: 5px;
        display: table;
        width: 100%;
        list-style: none;
    }

    .external-links li {
        display: table-cell;
        text-align: center;
    }


}
  </style>
</head>
<body style="background-color:#1f1f1f; color: #c6c6c6">

	<header style="display: flex; justify-content: center;">
           <img src="assets/images/games-365-top-logo.png" alt="header-image">   
    </header>

	<div class="top-tagline-banner">
		<div>
			<h1>Let the game begin</h1>
			<h4>with us, game in a whole another dimension</h4>
			<button onclick="location.href='login.html';">LOGIN</button>
			<button onclick="location.href='signup.html';">SIGNUP</button>
		</div>
		<div class="tagline-image" style="display: flex;justify-content: center;">
			<img src="assets/images/top-banner-bg.png" alt="tagline-image">
		</div>		
	</div>

	<div class="about-us" style="background-color:#282728">
		<h1>About Us</h1>
		<h4>We are internet gaming company, committed to deliver high quality content, to enthusiatic gamers, who WON'T let go</h4>	
	</div>

	<div class="pricing">
		<div> <h2 style="text-align:center; font-weight: bold; font-size: 2.0em;">Pricing</h2></div>
		<div class="pricing-cards-wrapper">
			
			<div class="pricing-card">
				<div class="pricing-cards-body">
					<div class="price-tag">FREE</div>
					<div>
						<ul>
							<li>Set up unlimited pages</li>
							<li>Get organic key requests</li>
							<li>Appear on discovery page</li>
							<li>Bragging rights</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="pricing-card">
				<div class="pricing-cards-body">
					<div class="price-tag">$99</div>
					<div>
						<ul>
							<li>Access to gold content</li>
							<li>In-game rewards worth $100</li>
							<li>Premium badges</li>
							<li>All benefits of FREE plan</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="pricing-card">
				<div class="pricing-cards-body">
					<div class="price-tag">$599</div>
					<div>
						<ul>
							<li>Access to platinum content</li>
							<li>In-game rewards worth $200</li>
							<li>Premium badges</li>
							<li>All benefits of $99 plan</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="pricing-card">
				<div class="pricing-cards-body">
					<div class="price-tag">$999</div>
					<div>
						<ul>
							<li>Access to diamond content</li>
							<li>In-game rewards worth $799</li>
							<li>Personal cloud gaming station</li>
							<li>All benefits of $599 plan</li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>


	<footer style="background-color: #222222;">
		<h4 style="text-align: center; margin: 20px">San Francisco</h4>
		<ul class="external-links">
            <li><a href="#">Creator Page</a></li>
			<li><a href="#">Team</a></li>
			<li><a href="#">Publisher FAQ</a></li>
			<li><a href="#">Eligibility</a></li>
			<li><a href="#">Terms of Service</a></li>
			<li><a href="#">Privacy Policy</a></li>
        </ul>
		<p style="text-align:center;">All Rights Reserved © Games 365 - 101 Superior Street San Francisco, Los Angeles, CA</p>
	</footer>

</body>
</html>
)=====";