#include <ESP8266WebServer.h>   //Library for handling WebServer APIs on ESP8266
#include <ESP8266WiFi.h>        //Library to handle WiFi and AccessPoint using ESP8266
#include <ESP8266HTTPClient.h>  //Library to transfer HyperText data over HTTP protocol
#include "html.h"


//---------------Variables declaration--------------------------------------------------------------
int statusCode;
String ipAddress = "";

//-------------Functions declaration---------------------------------------------------------------
void initializeComponents(void);
void listenClientRequests(void);
void launchWebServer(void);
void setupAccessPoint(void);
String ipToString(IPAddress ipAddress);

//-----------Objects declaration-------------------------------------------------------------------
ESP8266WebServer server(80);

void setup()
{
  initializeComponents();
  setupAccessPoint();
  launchWebServer();
}

void loop()
{
  listenClientRequests();
}
