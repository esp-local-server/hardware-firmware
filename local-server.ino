String ipToString(IPAddress ipAddress)
{
  return (String(ipAddress[0]) + '.' + String(ipAddress[1]) + '.' + String(ipAddress[2]) + '.' + String(ipAddress[3]));
}

void listenClientRequests()
{
  server.handleClient();
  
  if ((WiFi.status() != WL_CONNECTED))
  {
    Serial.println("Waiting.");
    Serial.print(".");
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(200);
  }
}

void launchWebServer()
{
  if (WiFi.status() == WL_CONNECTED)
    Serial.println("WiFi connected");

  Serial.println("Local IP: " + ipToString(WiFi.localIP()));
  Serial.println("SoftAP IP: " + ipToString(WiFi.softAPIP()));

  handleWebServerRoutes();
  server.begin();
  Serial.println("Server successfully hosted at 192.168.4.1");
}

void handleWebServerRoutes()
{
  server.on("/", indexPage);
}

void indexPage()
{
  String index = INDEX_HTML;
  server.send(200, "text/html", index);
}